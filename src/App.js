import React from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";

import {routes} from "./modules/routing/routing";

const WrapperRoute = () => {
  return(
      <Router>
        {routes.map( (route) => (
            <Route exact key={route.path} {...route} />
        ))}
      </Router>
  )
}
const App = () => {

  return (
      <div className="App">
        <WrapperRoute />
      </div>
  );
}

export default App;
