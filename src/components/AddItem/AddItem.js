import React, {Component} from 'react';
import './AddItem.css';

class AddItem extends Component {

    state = {
        label: ''
    }

    onLabelChange = (e) => {
        this.setState({
            label: e.target.value
        })
    }

    onSubmit = (event) => {
        event.preventDefault()
        const {label} = this.state;

        const cb =this.props.onItemAdded || (() => {})
        cb(label)
        this.setState({label: ''})
    }

    render() {
        return (
            <>
            <form action="" className="item-add-form "
                  onSubmit={this.onSubmit}
            >

                <input
                    type="text"
                    className="form-control"
                    placeholder="What need to be done"
                    onChange={this.onLabelChange}
                    value={this.state.label}

                />
                <div className="btn-group">
                    <button
                        type="button"
                        className="btn btn-outline-secondary"
                    >
                        Add New Item
                    </button>
                </div>

            </form>
                {this.state.label}
            </>
        );
    }
};

export default AddItem;
