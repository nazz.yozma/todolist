import React, {useState} from "react";
import NavMenu from "../NavMenu/NavMenu";
import {AppBar, IconButton, Toolbar, Typography} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';

const Header = () => {

    const [open, setOpen] = useState(false)
    //
    const onChangeOpen = () => setOpen(!open)

    return(
        <div>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="menu">
                        <MenuIcon  onClick={onChangeOpen} />
                    </IconButton>
                    <Typography variant="h6" >
                       To Do List
                    </Typography>

                </Toolbar>
            </AppBar>
            {open && <NavMenu/>}
            {/*<NavMenu/>*/}
        </div>
    )
}


export default Header;
