import React from 'react'
import {Link} from 'react-router-dom';

import './NavMenu.css'


const NavMenu = () => {
    // const history = useHistory();

    // const goToLink = (path: string) => {
    //     history.push(path)
    //     // console.log(path)
    // }

    // @ts-ignore

    return(
        <div className="NavMenu">
            <Link to="/">Dashboard</Link>
            <Link to="/starWars">User</Link>
        </div>
    )
}

export default NavMenu