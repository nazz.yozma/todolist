import React, {Component} from 'react';

export default class ItemStatusFilter extends Component {

    buttonsArr = [
        {name: 'all', label: 'All'},
        {name: 'active', label: 'Active'},
        {name: 'done', label: 'Done'},
    ]


    render() {
        const { filter } = this.props
        const buttons = this.buttonsArr.map( ({name, label}) => {
            const isActive = filter === name;
            const classBtn = isActive ? 'btn-info' : 'btn-outline-secondary'
            return(
                <button
                    key={name}
                    type="button"
                    className={`btn ${classBtn}`}>{label}</button>
            )
        })


        return (
            <div className="btn-group">
                { buttons }
                {/*{*/}
                {/*    buttons.map( ({name, label}) => {*/}
                {/*      return(*/}
                {/*          <button type="button"*/}
                {/*                  className="btn btn-info">All</button>*/}
                {/*      )*/}
                {/*    })*/}
                {/*// }*/}
                {/*// <button type="button"*/}
                {/*//         className="btn btn-info">All</button>*/}
                {/*// <button type="button"*/}
                {/*//         className="btn btn-outline-secondary">Active</button>*/}
                {/*// <button type="button"*/}
                {/*//         className="btn btn-outline-secondary">Done</button>*/}
            </div>
        );
    }


};
