import React, {Component} from 'react'
import './SearchPanel.css'
import ItemStatusFilter from "./ItemStatusFilter/ItemStatusFilter";

class SearchPanel extends Component {
    state = {
        term: ''
    }
    onSearchChange =  (e) => {
        const term = e.target.value
        this.setState({
            term
        })
        this.props.onSearchChange(term)
    }
    render() {
        const searchText = 'Type here to search'
        return(
            <>
                <div className="SearchPanel">
                    <input
                        type="text"
                        className="form-control search-input"
                        placeholder={searchText}
                        value={this.state.term}
                        onChange={this.onSearchChange}
                    />
                    <ItemStatusFilter filter={this.onSearch} />
                </div>
            </>
        )
    }
}

export default SearchPanel;