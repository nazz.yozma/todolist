import React from 'react'
import './ToDoList.css'
import SearchPanel from "../SearchPanel/SearchPanel";
import ToDoListItem from "./ToDoListItem/ToDoListItem";
import ToDoListHeader from "./ToDoListHeader/ToDoListHeader";

const ToDoList = ({ filter, todos, onDeleted, onToggleImportant, onToggleDone, doneCount, toDo, onSearchChange}) => {



    const elements = todos.map( (item) => {

        const {id, ...itemProps} = item

        return(
            <li key={id} className="list-group-item">
                <ToDoListItem
                    {...itemProps}
                    onDeleted={ () => onDeleted(id) }
                    onToggleImportant={ () => onToggleImportant(id) }
                    onToggleDone={ () => onToggleDone(id) }

                />
            </li>
        )
    })

    return(
        <div className="TodoList list-group" >
            <ToDoListHeader  toDo={toDo}  done={doneCount} />
            <SearchPanel onSearchChange={onSearchChange}  onSearchfilter={filter}/>
            <ul>
                {elements}
            </ul>
        </div>
    )
}

export default ToDoList;