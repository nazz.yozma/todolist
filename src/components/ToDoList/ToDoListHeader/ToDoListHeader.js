import React from 'react'

 const ToDoListHeader = ({toDo, done}) => {
    return(
        <>
            <h1>My Todo List</h1>
            <p><b>{toDo}</b> more to do, <b>{done}</b> is done</p>
        </>
    )
}
export default ToDoListHeader