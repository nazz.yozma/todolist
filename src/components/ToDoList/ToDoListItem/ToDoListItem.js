import React, {Component} from 'react'
import './ToDoListItem.css'

class ToDoListItem extends Component {

    render() {
        const {done, important, label , onDeleted, onToggleImportant, onToggleDone} = this.props


        let classNames = 'todo-list-item'

        if(done) {
            classNames += ' done';
        }

        if(important) {
            classNames += ' important'
        }

        return(
            <>
                <span
                    className={classNames}
                    onClick={onToggleDone}
                >
                    {label}
                </span>
                <button
                    type="button"
                    className="btn btn-outline-success float-right"
                    onClick={onToggleImportant}
                >
                    <i className="fa fa-exclamation" />
                </button>

                <button
                    type="button"
                    className="btn btn-outline-danger float-right"
                    onClick={onDeleted}

                >
                    <i className="fa fa-trash-o" />
                </button>
            </>
        )
    }
}

export default ToDoListItem;