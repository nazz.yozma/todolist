import React from 'react';

import './Header.css';
import {Link} from "react-router-dom";

const Header = () => {
    return (
        <div className="header d-flex">
            <h3>
                <a href="#">
                    StarDB
                </a>
            </h3>
            <ul className="d-flex">
                <Link to="/people">People</Link>
                <Link to="/planets">Planets</Link>
                <Link to="/starships">Starships</Link>
            </ul>
        </div>
    );
};

export default Header;