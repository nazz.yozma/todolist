export { default as People } from './People/People'
export { default as Planets } from './Planets/Planets'
export { default as Starships } from './Starships/Starships'