import ToDoApp from "../../views/ToDoApp/ToDoApp";
import StarWars from "../../views/StarWars/StarWars";
import {People, Planets, Starships} from "../../componentsStarWars";

export const routes = [
    {
        path: '/todo',
        component: ToDoApp,
        exact: true
    },
    {
        path: '/',
        component: StarWars,
        exact: true
    },


    {
        path: '/people',
        component: People,
        exact: true
    },
    {
        path: '/starship',
        component: Starships,
        exact: true
    },
    {
        path: '/planets',
        component: Planets,
        exact: true
    }
]