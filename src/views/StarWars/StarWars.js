import React, {Component} from 'react'
import Header from "../../componentsStarWars/Header/Header";
import './StarWars.css'
import '../../modules/services/swapi-service'

class StarWars extends Component{
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
        console.log('constructor(props)')
    }

    static getDerivedStateFromProps(props, state){
        console.log('pROPS', props)
        console.log('State', state)
        return( state)
    }
    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            10000
        );
        console.log('  componentDidMount()')
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
        console.log('   componentWillUnmount()')
    }

    tick() {
        this.setState({
            date: new Date()
        });
        console.log('  tick()')
    }
    render() {
        console.log('   render() ')
        return(
            <div>
                <h1>Привіт, світе!</h1>
                <h2>Зараз {this.state.date.toLocaleTimeString()}.</h2>
            </div>
        )
    }
}

export default StarWars;