import React, {Component} from 'react'
import ToDoList from "../../components/ToDoList/ToDoList";
import Header from "../../components/Header/Header";
import AddItem from "../../components/AddItem/AddItem";

// I would recommend you to not use Class Components
// Also, the code contains a lot of unused things, like lifecycle methods logging messages. 
// Please remove such things going forward
class ToDoApp extends Component {

    maxId = 1

    constructor() {
        super();

        this.state = {
            todoData: [
                this.createTodoItem('Wiggle Wiggle'),
                this.createTodoItem('Drink Coffe'),
                this.createTodoItem('Learn JS'),
                this.createTodoItem('Popka')
            ],
            term: '',
            filter: '' // aative / all / done

        }
        console.log('constructor()')
    }
    static getDerivedStateFromProps(props, state){
        console.log('Props:', props)
        console.log('State:', state)
        return state
    }


    componentDidMount() {

        console.log('componentDidMount()')
    }

    componentWillUnmount() {

        console.log('componentWillUnmount()')
    }

    shouldComponentUpdate(nextProps, nextState){
        console.log('nextProps', nextProps)
        console.log('nextState', nextState)
    }





    createTodoItem (label) {
        return {
            label: label,
            important: false,
            done: false,
            id: this.maxId++
        }
    }

    deleteItem = (id) => {
        console.log('del', id);

        this.setState( ({todoData}) => {
            const idx = todoData.findIndex( el => el.id === id )

            const newArray = [ ...todoData.slice(0, idx), ...todoData.slice(idx +1 ) ]
            return { todoData: newArray }
        })
    }

    addItem = (text) => {
        // console.log('added', text)

        const newItem = this.createTodoItem(text)

        this.setState( ({todoData}) => {
            const newArray = [ ...todoData , newItem]
            return { todoData: newArray }
        })

    }   


    toggleProperty (arr, id, propName) {
        const idx = arr.findIndex( (el) => el.id === id)

        const oldItem = arr[idx]
            console.log(oldItem)
        const val = !oldItem[propName]

        const newItem =  { ...arr[idx], [propName]: val}
            console.log(newItem)

        return{
            ...arr.slice(0, idx),
            newItem,
            ...arr.slice(idx + 1)
        }
    }


    onToggleImportant = (id) => {
        console.log('Important', id)
        // this.setState(({todoData}) => {
        //     const items = this.toggleProperty(todoData.items, id, 'important')
        //     return { items }
        //
        // })
    }

    onToggleDone = (id) => {
        console.log('done', id)
        this.setState(({todoData}) => {

            const idx = todoData.findIndex( (el) => el.id === id)

            const oldItem = todoData[idx]
                console.log(oldItem)

            const newItem =  { ...oldItem,
                done: !oldItem.done}
                console.log(newItem)

            const newArr = [
                ...todoData.slice(0, idx),
                newItem,
                ...todoData.slice(idx + 1)
            ]
                console.log(newArr)
            return{
                todoData: newArr
            }
        })

    }

    onSearchChange = (term) => {
        this.setState({ term })
    }

    search(items, term) {
        if(term.length === 0){
            return items
        }

        return items.filter((item) => {
            return item.label
                .toLowerCase()
                .indexOf(term.toLowerCase()) > -1
        })
    }

    filter(items, filter) {
        switch (filter) {
            case 'all':
                return items
            case 'active':
                return ( (item) => !item.done )
            case 'done':
                return ( (item) => item.done)
            default:
                return items;

        }
    }

  render() {

        console.log('render()')

      const { todoData, term, filter } = this.state;

      // const visibleItem = this.filter(
      //     this.search(todoData, term),
      //     filter
      // )


      const visibleItem = this.filter(
          this.search(todoData, term),
          filter
      )

      const doneCount = todoData.filter( (el) => el.done).length
      const toDoCount = todoData.length - doneCount

      return(
          <>
              <Header  />

              <div className="container">
                  <ToDoList
                      toDo={toDoCount}
                      doneCount={doneCount}
                      todos={visibleItem}
                      onDeleted={ this.deleteItem }
                      onToggleImportant={this.onToggleImportant}
                      onToggleDone={this.onToggleDone}
                      onSearchChange={this.onSearchChange}
                  />
                  <AddItem onItemAdded={this.addItem} />
              </div>
          </>
      )
  }
}

export default ToDoApp;